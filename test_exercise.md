# Exercise to evaluate the performance of leaners

The main object of this user interface is to help learners evaluate their performance, give them feedback on individual answers, and help learners guide the system to their weekness.  
From the system's perspective, this user interface can be used to collect data about the correctness of respones and confidence of learners to infer the correct answers to candidates to be added to the language resource.

## User Interface Sketch
The user interace 

![alt text](images/task3_test_exercise.jpg)


## Main discussion points


* What is the cognitive load of the screen?
* When to display these exercises in the system? Should it be the choice of learners and the system makes this decision?
* How many exercise items should be displayed in one screen?
* How many items with candidates for language resources?
* Should the correctness and confidence choices have default values?
* Which items are choosen to provide feedback to the learner?
    * In general, the items should be choosen randomly?
    * It might be better to give feedback to low-confidence and incorrect anwers?
    * How much weightage should be given to sampling of low-confidence and incorrect answers?