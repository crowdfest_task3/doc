## Add User
User is ready to get the next exercise, interface should send this request to the dispatcher.
```
add_user(userid, uname)
{
    "success": boolean,
}
```

## Get User Info
User is ready to get the next exercise, interface should send this request to the dispatcher.
```
get_user_info(userid)
{
    "userid": string,
    "uname": string,
    "timestamp": string (date format?)
}
```


## Get Exercise
User is ready to get the next exercise, interface should send this request to the dispatcher.
```
get_exercise(userid,elevel,etype)
{
    "eid": int,
    "userid": string,
    "exercise": string,
    "previous_responses": list of strings
}
elevel = 'A1','A2',...
etype = 'RelatedTo', 'AtLocation', 'PartOf'
```

## Store response
User has entered a response to an exercise, interface should send this request with the answer to the dispatcher.
```
store_response(eid,userid,response)
{
    "eid": int,
    "userid": string,
    "response": string,
    "points": int/null (null means pending evaluation),
}
```
*points*:
- _null_ means pending evaluation
- _int_ >0 for valid point, 0 means user has already provided this answer before

## Get random response
User doesn't know what to answer so we give a possible answer
```
get_random_response(eid, userid)
{
    "eid": int,
    "userid": string,
    "response": string,
}
```

## Get points
User wants to access info about her points
```
get_points(userid)
{
    "summary": {
        "earned_points": int,
        "potential_points": int,
        "badges": int, (number of time user was the first inputing a new "correct" response"
    }
    "responses": [
        {
            "eid": int,
            "userid": string,
            "response": string,
            "points": int/null (null means pending evaluation),
            "notified": boolean,
            "badge": boolean (true if this response is the first being validated as "correct")
        },
        ...
    ]
}
```

## Get Notifications
Client interface periodically asks to the dispatcher if a certain user has new notifications
```
get_notifications(userid)
{
    "responses": [
        {
            "eid": int,
            "userid": string,
            "timestamp": date, 
            "exercise": string,
            "response": string,
            "points": int,
            "badge": int
        },
        ...
    ]
}
```

## Leaderboard
User wants to access the leader board
```
get_leaderboard()
[
    {
        "userid": string,
        "uname": string,
        "rank": int,
        "earned_points": int,
        "potential_points": int,
        "badges": int
    },
    ...
]
```