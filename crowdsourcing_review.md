# Objective
A review of existing task (or question) designs for crowdsourcing in support of creation or curation of language resources.


# List of papers


| Paper | Language Resource | Task Design | Interaction Mechanism |
| ------ | ------ | ------ | ------ | 
| [Biemann (2013)](#Biemann2013) | [TWSI Turk Bootstrap Word Sense Inventory][twsi] | Keywords (max 5), Multiple Choice | Amazon Mechanical Turk |
| [Ganbold, Chagnaa, & Bella (2018)](#Ganbold2018) | WordNet Sysnet (English to Mogolian) | Multiple Choice | Amazon Mechanical Turk |


# Task Design

Close-ended tasks in response to questions related to a language resource can include:

* **Single Choice**: The task requires user to chose one between two more choices of possible answers. 
* **Multiple Choice**: The task requires the user to choose one or more among available choices. 
* **Ranking**: The task requires user to rank multiple choices according a predefined criteria.
* **Clustering**: The task requires user to group the available choices in two or more clusters based on some predefined criteria.

Open-ended tasks can include:

* **Keywords**: The task requires user to provide a set keywords (or tags, labels, stop words, etc).
* **Free Text**: The task requires user to enter an open text (such as translation, opinion, etc).
* **Free Speech**: The task requires user to provide audio recording.

# Definitions

| Term | Definition |
| ------ | ------ |
| Language resource | A speech or language dataset created and curated, in machine readable format, for the purpose of supporting software systems based on natural language processing, language learning, speech processing, and localization.  |
| Task | A language learning question provided to a learners as part of an excersice or a decision asked from a langguage experts or teachers. |
| Learning Excerise | A set of questions posed to a languge learners as part of improving and testing their capability of certain language elements. |
| User | A language learner, language expert or teacher. |



# Links
* http://www.elra.info/en/about/what-language-resource/
* https://www.zotero.org/groups/2174884/_enetcollect_bibliography_

# References
<a name="Biemann2013"></a> Biemann, C. (2013). Creating a system for lexical substitutions from scratch using crowdsourcing. Language Resources and Evaluation, 47(1), 97-122.

<a name="Ganbold2018"></a> Ganbold, A., Chagnaa, A., & Bella, G. (2018). Using Crowd Agreement for Wordnet Localization. In Proceedings of the Eleventh International Conference on Language Resources and Evaluation (LREC-2018).






[twsi]: https://www.inf.uni-hamburg.de/en/inst/ab/lt/resources/data/twsi-turk-bootstrap-word-sense-inventory.html