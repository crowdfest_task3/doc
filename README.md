# Vocabulary trainer with implicit crowdsourcing of language Resources (LRs)

Task 3 of the [CrowdFest Event](https://www.enetcollect.net/ilias/goto.php?target=wiki_546_crowdfest)

The overall aim is to design and implement a vocabulary trainer for language learners, which provides interactive vocabulary exercises like "fill the gap", "select all verbs among the given words", while crowdsourcing the learners’ answers. This vocabulary trainer will build on existing mono- and bilingual corpora or lexicons to generate exercise content. For example, a corpus can be used to create fill-the-gap exercises or a lexicon can be used to create exercises related to grammatical categories of words (e.g. gender, plural forms, etc.). The collection of learners’ answers will be used to extend or improve the language resources that the exercises are built from (e.g. by asking the user to choose the grammatical category of any word within a corpus that is not part of the lexicon, new lexicon entries can be created).

There are three aspects of the proposed systems:

* Improve an existing language resource by augmententing it with crowdsourced data.
    * What type of data the langauge resourcec contains?
    * What will be sources from langauge learners?
* Provide meaninful and engaging exercises to language learners who want to improve their vocabulary
    * What is learning outcome for learners?
    * How to engage learners in the exercise?
    * How to incentivize the leaners?
    * How to provide feedback to learners about their progress in terms of learning?
* Design a general approach for implicitly crowdsourcing data using open-ended questions?
    * How to get responses to open-ended questions?
    * What stopping criteria is used to decide when to evalute the response?
    * What agreement method is used to decide which response is correct?


## Architecture
![alt text](images/task3-architecture.png)

## User Interaction

#### Performing excersices

1. The user is entering the vocabulary trainer (VT)
2. The VT is prompting the user to "Name one thing that is related to X"
3. The user enters a response ("a thing related to X")
4. The VT is giving feedback on that response: the number of points / potential points and badges earned (see below)

Asynchronously, the user is notified about any potential points and badges as soon as they have been confirmed.

#### Checking points 

The user can request an overview of his performance, including

* the total amount of the user's points
* the total amount of the user's potential points
* the total number of the user's badges

#### Checking the leaderboard

The user can request an overview of the top performing users, including

* the user's name
* the user's points
* the user's badges

Checking your notifications:  
The user will receive a notification once response candidates are confirmed and the user receives instantiations of his potential points.
The user can request an overview of his latest gained points and badges (potential points turned into actual points)



## Response Evaluation
Responses are evaluated in relation to a specific user and a specific exercise (response, uid, eid).
The evaluation triggers two actions
* assignment of points and badges to the user
* possibly the assigment of points and badges for other users
* update of the list of correct responses for an exercise

#### Overview
```
IF a user is submitting a response that he had submitted before  
    the user receives 0 points (Case 1)  
ELSE the user is submitting a response for the first time  
    IF the response is part of the responses list  
        the user receives 1 point (Case 2)  
    ELSE the response is a new candidate  
        IF there are at least 6 new candidates in the list  
            IF the response is among the most frequent candidates  
                IF the response has been provided at least twice  
                    the user receives 2 points (Case 3)  
                    AND  
                    any other user that gave this same response receives 2 points (Case 3)  
                    AND  
                    the user who has provided this candidate the earlies receives 1 badge   
                ELSE the user receives 'potential points' notification / NULL (Case 4)  
            ELSE  
                the user receives a 'potential points' notification / NULL (Case 4)  
                IF the most frequent candidates occur at least twice  
                    any user providing the most frequent candidate receives 2 points (Case 3)  
                    AND  
                    the user who has provided this candidate the earlies receives 1 badge  
        ELSE the user receives a 'potential points' notification / NULL (Case 4)  
```       


Summary:  
* **0 points**: for any response that is submitted more than once (Case 1)
* **1 point**: for a response that is part of the responses list (Case 2)
* **2 points**: for a new response that is among the highest ranked candidates AND has been submitted by at least two users (Case 3)
* **No points** / NULL value (which can turn into 2 points later): for any new response that is not highest ranked or only occurs once (Case 4)
* **1 badge**: for the user that provided a new response the earliest and this response is among the highest ranked candidates AND has been submitted by at least two users (Case 3)

#### Normal Points

```mermaid

graph TD
 A(User response word received) -->B[Word previously submitted to same excessive?]
B--> |yes | C(Award user 0 points.)
B--> |No | D[Is the word part of exercise list?]
D--> |Yes | E(Award user 1 point.)
D--> |No | F[Add word to candidate list]
F--> G(Notify user about potential reward.)

```


#### Exercise Update and Delayed Points 

The exercise response set is updated whenever Case 3 occurs. The new response that is among the highest ranked candidates AND has been submitted by at least two users is added to the new responses set.

```mermaid
graph TD
 A(N candidate words received) -->C[Does any candidate have more than one vote?]
 C -->|Yes| D[Choose the update word with majority vote.]
 C -->|No| E(Ignore.)
D--> F[Award 2 points to all users who voted for update word.]
F--> G(Assign a badge for first user who voted for update word.)

```
# Publications
if you like this work and want to read more about it you can read the folowing publications on the architecture and experiments conducted using the V-trel Vocabulary Trainer:
>>>
[1] V. Lyding, C. Rodosthenous, F. Sangati, U. ul Hassan, L. Nicolas, A. König, J. Horbacauskiene, A. Katinskaia (2019). v-trel: Vocabulary Trainer for Tracing Word Relations - a Crowdsourcing Approach. (To appear) in Proceedings of the International Conference Recent Advances in Natural Language Processing (RANLP 2019), Varna, Bulgaria.
>>>
>>>
[2] C. Rodosthenous, V. Lyding, A. König, J. Horbacauskiene, A. Katinskaia, U. ul Hassan, N. Isaak, F. Sangati, L. Nicolas (2019). [Designing a Prototype Architecture for Crowdsourcing Language Resources](http://ceur-ws.org/Vol-2402/paper4.pdf). T. Declerck & J. P. McCrae (Ed.), in Proceedings of the Poster Session of the 2nd Conference on Language, Data and Knowledge (LDK 2019), pp. 17–23, CEUR, 2019.
>>>
# Other links
* [API documentation](https://crowdfest_task3.gitlab.io/api/)
* [API documentation (old)](api_doc.md)
* [Review of crowdsourcding](crowdsourcing_review.md)